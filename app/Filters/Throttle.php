<?php 
namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;

class Throttle implements FilterInterface
{
	/**
	 * 这是一个为应用程序使用 Trottler 类来实现速率限制的实例
	 *
	 * @param RequestInterface|\CodeIgniter\HTTP\IncomingRequest $request
	 *
	 * @return mixed
	 */
	public function before(RequestInterface $request)
	{
		$throttler = Services::throttler();

		// 在整个站点上将IP地址限制为每秒不超过1个请求
		// 第一个参数是存储桶名称
		// 第二个参数是存储桶持有的令牌数量
		// 第三个参数是存储桶重新填充所需的时间
		if ($throttler->check($request->getIPAddress(), 60, MINUTE) === false)
		{
			return Services::response()->setStatusCode(429);
		}
	}

	//--------------------------------------------------------------------

	/**
	 * 暂时无事可做
	 *
	 * @param RequestInterface|\CodeIgniter\HTTP\IncomingRequest $request
	 * @param ResponseInterface|\CodeIgniter\HTTP\Response       $response
	 *
	 * @return mixed
	 */
	public function after(RequestInterface $request, ResponseInterface $response)
	{
	}
}